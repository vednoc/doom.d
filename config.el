;;; -*- lexical-binding: t; -*-

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(global-git-gutter-mode t)
(global-prettify-symbols-mode)

;; Variables.
(setq display-line-numbers-type nil

      user-full-name "vednoc"
      user-mail-address "vednoc@protonmail.com"

      ;; Tweaks for large fonts. Sacrifices resources for performance.
      inhibit-compacting-font-caches t

      scroll-margin 3
      scroll-step 1
      scroll-conservatively 10000
      scroll-preserve-screen-position 1

      window-divider-default-bottom-width 0

      git-gutter:update-interval 2
      git-gutter:window-width -1

      ;; Fast loading of git commit details.
      magit-revision-show-gravatars nil

      doom-modeline-major-mode-icon 1

      doom-theme 'doom-gruvbox-dark-hard

      doom-font (font-spec :family "Iosevka Slab" :size 16)
      doom-big-font (font-spec :family "Iosevka Slab" :size 20)
      doom-unicode-font(font-spec :family "Dejavu Sans Mono" :size 16)
      doom-variable-pitch-font(font-spec :family "Iosevka Etoile")

      ;; Escape to `normal' mode using `jk' or `kj'.
      evil-escape-unordered-key-sequence t)

;; Word wrap.
(add-hook 'prog-mode-hook #'visual-line-mode t)

;; Rust configs.
(set-formatter! 'rustic-mode #'rustic-cargo-fmt)
(setq lsp-rust-server 'rust-analyzer
      rustic-lsp-server 'rust-analyzer
      rustic-format-on-save t)

;; Stylus configs.
(add-hook 'stylus-mode-hook (lambda ()
                              (hl-todo-mode t)
                              (visual-line-mode t)
                              (rainbow-delimiters-mode t)))

;; Character used when rendering indent guides.
(after! highlight-indent-guides
  (remove-hook! 'prog-mode-hook 'highlight-indent-guides-mode)
  (setq highlight-indent-guides-responsive 'top
        ;; ⎸ ┆ ┊ ¦ │ ▏
        highlight-indent-guides-character ?\┊))

(after! doom-modeline
  (setq doom-modeline-buffer-file-name-style 'relative-to-project))

;; Org-mode configs.
(after! org
  (setq org-log-done 'time
        org-ellipsis "..."
        org-superstar-headline-bullets-list '("☰" "☱" "☲" "☳" "☴" "☵" "☶" "☷" "☷" "☷" "☷")
        org-superstar-prettify-item-bullets t
        org-superstar-special-todo-items t
        org-hide-emphasis-markers t
        org-pretty-entities t
        org-tags-column -77
        hl-line-mode nil
        org-directory "~/doc/org"))

;; Fix hardcoded tab-width value in Jade mode; found in Stylus files as well.
(setq sws-tab-width 4)

;; https://github.com/hlissner/emacs-solaire-mode/issues/21
(after! treemacs
  (advice-add '+treemacs--init :after #'balance-windows))

(add-hook 'crystal-mode-hook
          (lambda () (add-hook 'before-save-hook #'crystal-tool-format)))

;; Custom keybindings.
(map! :ne "M-[" #'+workspace:switch-previous
      :ne "M-]" #'+workspace:switch-next
      :ne "M-l" #'+workspace/other
      :ne "g ," #'git-gutter:next-hunk
      :ne "g ." #'git-gutter:previous-hunk
      :nei "M-/" #'evilnc-comment-or-uncomment-lines)

;; Adding missing motions to Magit.
(map! :map magit-mode-map
      :n  "gi" #'forge-jump-to-issues
      :n  "gm" #'forge-jump-to-pullreqs
      :nv "zt" #'evil-scroll-line-to-top
      :nv "zb" #'evil-scroll-line-to-bottom)

(map! :leader
      :prefix "TAB"
      :n "j" #'+workspace/swap-left
      :n "k" #'+workspace/swap-right

      :prefix "t"
      :desc "Keycast mode" "k" #'keycast-mode)

(defun v/wider-fringes ()
  (when (window-system)
    (setq left-fringe-width 12)))

(add-hook! 'magit-status-mode-hook #'v/wider-fringes)
